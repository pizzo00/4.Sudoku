//
//  main.m
//  4.Sudoku
//
//  Created by DavideMac on 24/01/17.
//  Copyright © 2017 Davide Pizzolato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));    }
}
