//
//  Gioco.h
//  4.Sudoku
//
//  Created by DavideMac on 24/01/17.
//  Copyright © 2017 Davide Pizzolato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Gioco : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btt_nuovaPartita;
@property (weak, nonatomic) IBOutlet UIButton *btt_riprendiPartitaSalvata;
@property (weak, nonatomic) IBOutlet UIButton *btt_controlla;
@property (weak, nonatomic) IBOutlet UIButton *btt_miArrendo;
@property (weak, nonatomic) IBOutlet UIButton *btt_1;
@property (weak, nonatomic) IBOutlet UIButton *btt_2;
@property (weak, nonatomic) IBOutlet UIButton *btt_3;
@property (weak, nonatomic) IBOutlet UIButton *btt_4;
@property (weak, nonatomic) IBOutlet UIButton *btt_5;
@property (weak, nonatomic) IBOutlet UIButton *btt_6;
@property (weak, nonatomic) IBOutlet UIButton *btt_7;
@property (weak, nonatomic) IBOutlet UIButton *btt_8;
@property (weak, nonatomic) IBOutlet UIButton *btt_9;
@property (weak, nonatomic) IBOutlet UIButton *btt_casellaVuota;
@property (weak, nonatomic) IBOutlet UIImageView *img_sfondo;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment_selectSudoku;
@property (weak, nonatomic) IBOutlet UILabel *lbl_verde;
@property (weak, nonatomic) IBOutlet UILabel *lbl_esatto;
@property (weak, nonatomic) IBOutlet UILabel *lbl_rosso;
@property (weak, nonatomic) IBOutlet UILabel *lbl_errato;
@property (weak, nonatomic) IBOutlet UIButton *btt_nuovaPartitaArreso;

- (IBAction)cominciaNuovaPartita:(id)sender;
- (IBAction)riprendiPartitaSalvata:(id)sender;
- (IBAction)controlla:(id)sender;
- (IBAction)completaSudoku:(id)sender;
- (IBAction)numeroCliccato:(id)sender;
- (IBAction)segmentSudokuChange:(id)sender;

@end
