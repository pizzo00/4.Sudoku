//
//  Istruzioni.m
//  4.Sudoku
//
//  Created by Studente on 15/02/17.
//  Copyright © 2017 Davide Pizzolato. All rights reserved.
//

#import "Istruzioni.h"
#import "Define.h"

@interface Istruzioni ()

@end

@implementation Istruzioni

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Define settaSfondo:_img_sfondo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
