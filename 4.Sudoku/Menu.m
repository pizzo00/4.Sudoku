//
//  Menu.m
//  4.Sudoku
//
//  Created by DavideMac on 25/01/17.
//  Copyright © 2017 Davide Pizzolato. All rights reserved.
//

#import "Menu.h"
#import "Define.h"

@interface Menu ()

@end

@implementation Menu


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self inizializzaVariabiliPermanenti];
    [Define settaSfondo:_img_sfondo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)inizializzaVariabiliPermanenti {
    if([self eLaPrimaVolta]) //setta le variabili permanenti alla prima apertura dell'app
    {
        NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
        //gioco
        [variabiliPermanenti setBool:YES   forKey:@"appGiaAperta"];
        [variabiliPermanenti setBool:NO    forKey:@"aiutiMostrati"];
        [variabiliPermanenti setBool:NO    forKey:@"presentePartitaSalvata"];
        //opzioni
        [variabiliPermanenti setInteger:SFONDO_1 forKey:@"sfondo"];
        [variabiliPermanenti setInteger:DIFFICOLTA_FACILE forKey:@"difficolta"];
        //statistiche
        [Define cancellaStatistiche];
        [variabiliPermanenti setInteger:0 forKey:@"partiteGiocate"];
        [variabiliPermanenti setInteger:0 forKey:@"partiteVinte"];
        [variabiliPermanenti synchronize];
    }
}

-(bool)eLaPrimaVolta{
    NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    return ![variabiliPermanenti objectForKey:@"appGiaAperta"];
    //return true;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
